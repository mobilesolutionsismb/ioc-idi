from .base import *

DEBUG = False

STATIC_ROOT = '/code/static/'
STATIC_URL = '/static/'

DATABASES = {
    'default': {
        'ENGINE': 'idi_api',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
        'OPTIONS': {
            'driver': 'ODBC Driver 13 for SQL Server',
            'MARS_Connection': 'True',
        }
    }
}


AZURE_BLOB_STORAGE = ""
AZURE_BLOB_STORAGE_ACCOUNT_NAME = ""
AZURE_BLOB_STORAGE_ACCOUNT_KEY = ""
AZURE_BLOB_STORAGE_CONTAINER = ""

AZURE_SERVICE_BUS_NAMESPACE = ""
AZURE_SERVICE_BUS_TOPIC_NAME = ""
AZURE_SERVICE_BUS_KEY_NAME = ""
AZURE_SERVICE_BUS_ACCESS_KEY = ""

BASE_ENV = "https://backend.ireact.cloud"
IREACT_ENUMERATORS = BASE_ENV + "/api/services/app/Enums/GetEnums"

IDI_API_LOG_PATH = "../idi_api_log.log"
