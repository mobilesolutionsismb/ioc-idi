from .base import *

DEBUG = True
CORS_ORIGIN_ALLOW_ALL = True

DATABASES = {
    'default': {
        'ENGINE': 'idi_api',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
        'OPTIONS': {
            'driver': 'ODBC Driver 13 for SQL Server',
            'MARS_Connection': 'True',
        }
    }
}

AZURE_BLOB_STORAGE = ""  # es: https://ireactdev.blob.core.windows.net/idi/
AZURE_BLOB_STORAGE_ACCOUNT_NAME = ""
AZURE_BLOB_STORAGE_ACCOUNT_KEY = ""
AZURE_BLOB_STORAGE_CONTAINER = ""

AZURE_SERVICE_BUS_NAMESPACE = ""
AZURE_SERVICE_BUS_TOPIC_NAME = ""
AZURE_SERVICE_BUS_KEY_NAME = ""
AZURE_SERVICE_BUS_ACCESS_KEY = ""

BASE_ENV = "https://backend-dev.ireact.cloud"
IREACT_ENUMERATORS = BASE_ENV + "/api/services/app/Enums/GetEnums"

IDI_API_LOG_PATH = "../idi_api_log.log"