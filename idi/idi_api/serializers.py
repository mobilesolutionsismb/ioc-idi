from idi_api.models import Metadata, Vocabulary
from rest_framework import serializers
from iso639 import languages
import iso8601
from django.conf import settings
import requests
import json
import re
from django.utils import timezone


class GeographicBoundingBoxSerializer(serializers.Serializer):
    north = serializers.FloatField()
    south = serializers.FloatField()
    west = serializers.FloatField()
    east = serializers.FloatField()


class SoftDeleteSerializer(serializers.Serializer):
    metadataid = serializers.IntegerField()


class QueryBuilderSerializer(serializers.Serializer):
    query = serializers.JSONField(help_text="The query")


class MultiFilesSerializer(serializers.Serializer):
    # fields not present in the IrMetadata model. Used here just for validation
    leadtimes = serializers.JSONField(required=False)
    timespans = serializers.JSONField(required=False)
    visualization_order = serializers.JSONField(required=False)
    filenames = serializers.JSONField(required=False)

    def validate_visualization_order(self, value):
        """
        Check visualization_order is a valid list
        """
        if isinstance(value, str):
            try:
                value = json.loads(value)
            except ValueError:
                raise serializers.ValidationError("Field requires a proper list")
        if not isinstance(value, list):
            raise serializers.ValidationError("Field requires a proper list")
        return value

    def validate_leadtimes(self, value):
        """
        Check leadtimes is a valid list of timestamps with proper format
        """
        if isinstance(value, str):
            try:
                value = json.loads(value)
            except ValueError:
                raise serializers.ValidationError("Must provide a proper list of timestamps with format %Y%m%dT%H%M%S")
        if not isinstance(value, list):
            raise serializers.ValidationError("Field requires a valid list of timestamps")
        for k, _value in enumerate(value):
            try:
                # check that the timestamp is in proper format
                value[k] = iso8601.parse_date(_value).strftime("%Y%m%dT%H%M%S")
            except iso8601.ParseError:
                raise serializers.ValidationError("Some leadtimes are not in the proper format: %Y%m%dT%H%M%S")
        return value

    def validate_timespan(self, value):
        """
        Check timespan is a valid list of timestamps with proper format
        """
        if isinstance(value, str):
            try:
                value = json.loads(value)
            except ValueError:
                raise serializers.ValidationError("Must provide a proper list of timestamps \
                                                (a timestamp is a float number >= 0)")
        if not isinstance(value, list):
            raise serializers.ValidationError("Field requires a valid list of timestamps")
        # check every timestamp is either an int or a float
        if not all(map(lambda x: (isinstance(x, int) or isinstance(x, float)) and x >= 0, value)):
            raise serializers.ValidationError("Some timespans are not in the proper format: float >= 0")
        return value


class MetadataSerializer(serializers.ModelSerializer):
    
    def __init__(self, *args, **kwargs):
        super(MetadataSerializer, self).__init__(*args, **kwargs)
        response = requests.get(settings.IREACT_ENUMERATORS)
        if response.status_code != 200:
            raise Exception("Endpoint " + settings.IREACT_ENUMERATORS +
                            " is not responding correctly. Operation failed.")
        self.enumerators = json.loads(response.text)['result']

    identification_resourcetype = serializers.CharField(max_length=250)
    classification_topiccategory = serializers.CharField(max_length=250)
    classification_spatialdataservicetype = serializers.CharField(max_length=250)
    keyword_originatingcontrolledvocabulary = serializers.CharField(max_length=250)
    qualityandvalidity_spatialresolution_measureunit = serializers.CharField(max_length=250)
    conformity_degree = serializers.CharField(max_length=250)
    responsibleorganization_responsiblepartyrole = serializers.CharField(max_length=250)
    qualityandvalidity_spatialresolution_latitude = serializers.FloatField(default=None)
    qualityandvalidity_spatialresolution_longitude = serializers.FloatField(default=None)
    qualityandvalidity_spatialresolution_scale = serializers.IntegerField(default=None)

    class Meta:
        model = Metadata
        # include all fields from the model
        # fields = '__all__'
        # we need to exclude some fields form the Metadata model because
        # they are stored in the db as Integers, whereas we expect strings
        # form the client which have to be mapped according to some enumerators.
        # These fields are redefined above as CharFields to be accepted by the
        # serializer.
        exclude = ('identification_resourcetype', 'conformity_degree',
                   'classification_topiccategory', 'classification_spatialdataservicetype',
                   'keyword_originatingcontrolledvocabulary',
                   'qualityandvalidity_spatialresolution_measureunit',
                   'responsibleorganization_responsiblepartyrole',
                   'qualityandvalidity_spatialresolution_latitude',
                   'qualityandvalidity_spatialresolution_longitude',
                   'qualityandvalidity_spatialresolution_scale')
        # autoincrement primary key, we should not modify this
        read_only_fields = ('id',)

    def validate_identification_resourcetype(self, value):
        """
        Check the resource_type. It should be just some text
        """
        return self.validation_enumerator('resourceType', value)

    # def validate_identification_coupledresource(self, value):
    #     """
    #     Check the coupled_resource. It should be just some text
    #     """
    #     # TODO: keep empty in the SQL??
    #     return value

    def validate_identification_resourcelanguage(self, value):
        """
        Check the resource_language. It should be a code compliant to ISO 639-2
        """
        # TODO get all ISO 639-2(B) codes - check this is ok
        codes = languages.part2b.keys()
        if value in codes:
            return value
        else:
            raise serializers.ValidationError("Must respect ISO 639-2 code")

    def validate_classification_topiccategory(self, value):
        """
        Check the topic_category. It should be a code compliant to ISO 19115
        """
        return self.validation_enumerator('topicCategory', value)

    def validate_classification_spatialdataservicetype(self, value):
        """
        Check the spatial_data_service_type. It should be just 'view'
        """
        return self.validation_enumerator('spatialDataServiceType', value)

    def validate_keyword_originatingcontrolledvocabulary(self, value):
        """
        Check the originating_controlled_vocabulary. It should be just some text 
        """
        return self.validation_enumerator('originatingControlledVocabulary', value)

    def validate_keyword_keywordvalue(self, value):
        """
        Check the keyword_value.
        """
        if "keyword_originatingcontrolledvocabulary" not in self.initial_data:
            raise serializers.ValidationError("Yuo can not insert keyword values without specifying a vocabulary")
        
        # match words separated by commas, if there is no match returns None
        match = re.search(r"^[a-zA-z\-\s\/]+(,[a-zA-z\-\s\/]+)*$", value)
        if match is not None:
            # check the words are allowed
            words = value.split(',')
            if len(words) > 5:
                raise serializers.ValidationError("Field requires a string of words separated by comma. At least 1, max 5")
            # the originating_controlled_vocabulary has already been checked
            voc = self.initial_data['keyword_originatingcontrolledvocabulary']
            # get the integer enum of the vocabulary
            voc_id = self.validation_enumerator('originatingControlledVocabulary', voc)
            # retrieve all available words for the specified vocabulary
            keywords = list(map(lambda x: x['keyword'],
                            Vocabulary.objects.filter(originatingcontrolledvocabulary=voc_id).values('keyword')))
            # in the case the specified vocabulary has not specified words, raise error.
            if len(keywords) == 0:
                raise serializers.ValidationError("Vocabulary {} has no words defined yet, use another one".format(voc))
            for w in words:
                # in the case we find a word not specified in the vocabulary, raise error
                if w not in keywords:
                    raise serializers.ValidationError("Words for vocabulary {} must be: {}".format(voc, keywords))
        else:
            raise serializers.ValidationError("Field requires a list of words separated by comma. At least 1, max 5")
        return value

    def validate_qualityandvalidity_spatialresolution_measureunit(self, value):
        """
        Check the measure_unit
        """
        return self.validation_enumerator('measureUnit', value)

    def validate_conformity_degree(self, value):
        """
        Check the degree
        """
        return self.validation_enumerator('degree', value)

    def validate_constraints_conditionsforaccessanduse(self, value):
        """
        Check conditions_for_access_and_use 
        """
        # TODO: is there a list of defined conditions? Free Text? > for now ok like this
        return value

    def validate_constraints_limitationsonpublicaccess(self, value):
        """
        Check limitations_on_public_access 
        """
        # TODO: Free text? > for now ok like this
        return value

    def validate_responsibleorganization_responsiblepartyrole(self, value):
        """
        Check responsible_organization_responsible_party_role
        """
        return self.validation_enumerator('responsiblePartyRole', value)

    def validate_layerattributes(self, value):
        """
        Check layerattributed is a valid JSON
        """
        try:
            j = json.loads(value)
        except ValueError:
            raise serializers.ValidationError("Field requires a valid JSON")
        return value

    def validate_creationtime(selfs, value):
        """
        Set the creationtime to now if it was passed form the client
        """
        if value is not None:
            raise serializers.ValidationError("Client can not send reserved fields: "
                                              "CreationTime, LastModificationTime, IsDeleted, DeletionTime")
        # return timezone.now()
        return value

    def validate_deletiontime(self, value):
        if value is not None:
            raise serializers.ValidationError("Client can not send reserved fields: "
                                              "CreationTime, LastModificationTime, IsDeleted, DeletionTime")
        return value

    # def validate_isdeleted(self, value):
    #     if value is not None:
    #         raise serializers.ValidationError("Client can not send reserved fields: "
    #                                           "CreationTime, LastModificationTime, IsDeleted, DeletionTime")
    #     return value

    def validate_lastmodificationtime(self, value):
        if value is not None:
            raise serializers.ValidationError("Client can not send reserved fields: "
                                              "CreationTime, LastModificationTime, IsDeleted, DeletionTime")
        return value

    def validation_enumerator(self, field_name, value):
        allowed = self.enumerators[field_name]
        if value in list(allowed.keys()):
            return allowed[value]  # return the corresponding integer of the enumerator
        else:
            raise serializers.ValidationError(
                "field_name must be one of {}".format(list(allowed.keys())))


class CreateMetadataSerializer(MetadataSerializer):

    def __init__(self, *args, **kwargs):
        super(CreateMetadataSerializer, self).__init__(*args, **kwargs)

    acquisitiondate = serializers.DateTimeField(required=True)

    def validate(self, data):
        # RULE: if scale is null, then lat res and lo res should be not null, and viceversa.
        # so if scale is null, then the other two values cannot be null
        # else: if scale is not null then the other values can be whatever they want
        if data['qualityandvalidity_spatialresolution_scale'] is None:
            if data['qualityandvalidity_spatialresolution_longitude'] is None \
                    or data['qualityandvalidity_spatialresolution_latitude'] is None:
                raise serializers.ValidationError(
                    "If scale is null, then latitude and longitude resolution should be not null, and vice versa ")

        # RULE: if TemporalReference_Start is equal to TemporalReference_End, then AcquisitionData has to be
        #       equal to TemporalReference_Start/End (the field is not mandatory)
        if 'acquisitiondate' in data:
            if data['temporalreference_start'] == data['temporalreference_end']:
                data['acquisitiondate'] = data['temporalreference_start']

            if data['acquisitiondate'] < data['temporalreference_start'] or \
                data['acquisitiondate'] > data['temporalreference_end']:
                raise serializers.ValidationError(
                    "Acquisition date must be a valid date between TemporalReference_Start and TemporalReference_End")
        return data


class VerboseMetadataSerialized(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        super(VerboseMetadataSerialized, self).__init__(*args, **kwargs)
        response = requests.get(settings.IREACT_ENUMERATORS)
        if response.status_code != 200:
            raise Exception("Endpoint " + settings.IREACT_ENUMERATORS +
                            " is not responding correctly. Operation failed.")
        self.enumerators = json.loads(response.text)['result']

    identification_resourcetype = serializers.CharField(max_length=250)
    classification_topiccategory = serializers.CharField(max_length=250)
    classification_spatialdataservicetype = serializers.CharField(max_length=250)
    keyword_originatingcontrolledvocabulary = serializers.CharField(max_length=250)
    qualityandvalidity_spatialresolution_measureunit = serializers.CharField(max_length=250)
    conformity_degree = serializers.CharField(max_length=250)
    responsibleorganization_responsiblepartyrole = serializers.CharField(max_length=250)

    class Meta:
        model = Metadata
        # include all fields from the model
        # fields = '__all__'
        # we need to exclude some fields form the Metadata model because
        # they are stored in the db as Integers, whereas we expect strings
        # form the client which have to be mapped according to some enumerators.
        # These fields are redefined above as CharFields to be accepted by the
        # serializer.
        exclude = ('identification_resourcetype',
                   'classification_topiccategory', 'classification_spatialdataservicetype',
                   'keyword_originatingcontrolledvocabulary',
                   'qualityandvalidity_spatialresolution_measureunit',
                   'conformity_degree',
                   'responsibleorganization_responsiblepartyrole',
                   )
        # autoincrement primary key, we should not modify this
        read_only_fields = ('id',)

    def validate_identification_resourcetype(self, value):
        """
        Check the resource_type. It should be just some text
        """
        return self.get_enumerator_value('resourceType', value)

    def validate_classification_topiccategory(self, value):
        """
        Check the topic_category. It should be a code compliant to ISO 19115
        """
        return self.get_enumerator_value('topicCategory', value)

    def validate_classification_spatialdataservicetype(self, value):
        """
        Check the spatial_data_service_type. It should be just 'view'
        """
        return self.get_enumerator_value('spatialDataServiceType', value)

    def validate_keyword_originatingcontrolledvocabulary(self, value):
        """
        Check the originating_controlled_vocabulary. It should be just some text
        """
        return self.get_enumerator_value('originatingControlledVocabulary', value)

    def validate_qualityandvalidity_spatialresolution_measureunit(self, value):
        """
        Check the measure_unit
        """
        return self.get_enumerator_value('measureUnit', value)

    def validate_conformity_degree(self, value):
        """
        Check the degree
        """
        return self.get_enumerator_value('degree', value)

    def validate_responsibleorganization_responsiblepartyrole(self, value):
        """
        Check responsible_organization_responsible_party_role
        """
        return self.get_enumerator_value('responsiblePartyRole', value)

    def get_enumerator_value(self, field_name, value):
        value = int(value)
        d = self.enumerators[field_name]
        # invert keys and values in dict
        values = dict([(v, k) for k, v in d.items()])
        if value in list(values.keys()):
            return values[value]  # return the corresponding value
        else:
            raise serializers.ValidationError(
                "field_name must be one of {}".format(list(values.keys())))
