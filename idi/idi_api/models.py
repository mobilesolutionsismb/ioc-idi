from django.utils import timezone

from django.db import models
from django.contrib.gis.db import models

from django.conf import settings


# class Constraints(models.Model):
#     field = models.CharField(max_length=40)
#     values = jsonfield.JSONField()
#

class MetadataManager(models.Manager):
    def of(self, user, *args, **kwargs):
        """
        Returns metadata owned by the user
        """
        return self.filter(ownership__owner=user, *args, **kwargs)  # todo: filter out soft-deleted ones?


class Metadata(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)
    layerattributes = models.TextField(db_column='LayerAttributes', blank=True, null=True)  # new
    identification_resourcetitle = models.TextField(db_column='Identification_ResourceTitle')  # , blank=True, null=True?
    identification_resourceabstract = models.TextField(db_column='Identification_ResourceAbstract')
    identification_resourcetype = models.IntegerField(db_column='Identification_ResourceType')
    identification_coupledresource = models.TextField(db_column='Identification_CoupledResource', blank=True, null=True)
    identification_resourcelanguage = models.CharField(db_column='Identification_ResourceLanguage', max_length=3)
    classification_topiccategory = models.IntegerField(db_column='Classification_TopicCategory', blank=True, null=True)
    classification_spatialdataservicetype = models.IntegerField(db_column='Classification_SpatialDataServiceType')
    keyword_keywordvalue = models.TextField(db_column='Keyword_KeywordValue')
    keyword_originatingcontrolledvocabulary = models.IntegerField(db_column='Keyword_OriginatingControlledVocabulary')
    temporalreference_start = models.DateTimeField(db_column='TemporalReference_Start')
    temporalreference_end = models.DateTimeField(db_column='TemporalReference_End')
    temporalreference_dateofpublication = models.DateTimeField(db_column='TemporalReference_DateOfPublication')
    temporalreference_dateoflastrevision = models.DateTimeField(db_column='TemporalReference_DateOfLastRevision')
    temporalreference_dateofcreation = models.DateTimeField(db_column='TemporalReference_DateOfCreation', default=timezone.now)
    qualityandvalidity_lineage = models.TextField(db_column='QualityAndValidity_Lineage', blank=True, null=True)
    qualityandvalidity_spatialresolution_latitude = models.FloatField(db_column='QualityAndValidity_SpatialResolution_Latitude', blank=True, default=None)
    qualityandvalidity_spatialresolution_longitude = models.FloatField(db_column='QualityAndValidity_SpatialResolution_Longitude', blank=True, default=None)
    qualityandvalidity_spatialresolution_scale = models.IntegerField(db_column='QualityAndValidity_SpatialResolution_Scale', blank=True, default=None)
    qualityandvalidity_spatialresolution_measureunit = models.IntegerField(db_column='QualityAndValidity_SpatialResolution_MeasureUnit')
    conformity_specification = models.TextField(db_column='Conformity_Specification')
    conformity_degree = models.IntegerField(db_column='Conformity_Degree')
    constraints_conditionsforaccessanduse = models.TextField(db_column='Constraints_ConditionsForAccessAndUse', blank=True, null=True)
    constraints_limitationsonpublicaccess = models.TextField(db_column='Constraints_LimitationsOnPublicAccess', blank=True, null=True)
    responsibleorganization_responsibleparty_organizationname = models.TextField(db_column='ResponsibleOrganization_ResponsibleParty_OrganizationName')
    responsibleorganization_responsibleparty_email = models.TextField(db_column='ResponsibleOrganization_ResponsibleParty_Email')
    responsibleorganization_responsiblepartyrole = models.IntegerField(db_column='ResponsibleOrganization_ResponsiblePartyRole')
    metadataonmetadata_pointofcontact_organizationname = models.TextField(db_column='MetadataOnMetadata_PointOfContact_OrganizationName')
    metadataonmetadata_pointofcontact_email = models.TextField(db_column='MetadataOnMetadata_PointOfContact_Email')
    metadataonmetadata_date = models.DateTimeField(db_column='MetadataOnMetadata_Date', default=timezone.now)
    metadataonmetadata_language = models.CharField(db_column='MetadataOnMetadata_Language', max_length=3)
    coordinatesystemreference_code = models.TextField(db_column='CoordinateSystemReference_Code')
    coordinatesystemreference_codespace = models.TextField(db_column='CoordinateSystemReference_CodeSpace')
    ireacttask = models.IntegerField(db_column='IreactTask')
    metadatafileuri = models.TextField(db_column='MetadataFileUri', blank=True, null=True)
    sourcenames = models.TextField(db_column='SourceNames', blank=True, null=True)
    acquisitiondate = models.DateTimeField(db_column='AcquisitionDate', blank=True, null=True)
    creationtime = models.DateTimeField(db_column='CreationTime', default=timezone.now)
    lastmodificationtime = models.DateTimeField(db_column='LastModificationTime', blank=True, null=True)
    isdeleted = models.BooleanField(db_column='IsDeleted', default=False)
    deletiontime = models.DateTimeField(db_column='DeletionTime', blank=True, null=True)
    geographicboundingboxes = models.TextField(db_column='GeographicBoundingBoxes', blank=True, default="MULTIPOLYGON EMPTY")

    objects = MetadataManager()

    def __str__(self):
        return '%s %s' % (self.id, self.identification_resourcetitle)

    class Meta:
        managed = False
        db_table = 'Metadata'
        verbose_name = 'Metadata entry'
        verbose_name_plural = 'Metadata entries'


class MetadataOwnership(models.Model):
    """
    Represents the relationship between the metadata and the user that created it
    """
    metadata = models.ForeignKey(Metadata, related_name='ownership', unique=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)  # todo: cascade?

    def __str__(self):
        return '%s belongs to %s' % (self.metadata, self.owner)


class Metadatafiles(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)
    filename = models.TextField(db_column='Filename', blank=True, null=True)
    leadtime = models.DateTimeField(db_column='LeadTime', blank=True, null=True)
    visualizationorder = models.IntegerField(db_column='VisualizationOrder', blank=True, null=True)
    metadataid = models.ForeignKey('idi_api.Metadata', models.DO_NOTHING, db_column='MetadataId')
    uri = models.TextField(db_column='Uri')
    timespan = models.FloatField(db_column='Timespan', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MetadataFiles'


class Vocabulary(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True)
    keyword = models.TextField(db_column='Keyword', blank=True, null=True)
    description = models.TextField(db_column='Description', blank=True, null=True)
    originatingcontrolledvocabulary = models.IntegerField(db_column='OriginatingControlledVocabulary', default=1)
    isdeleted = models.BooleanField(db_column='IsDeleted')
    deleteruserid = models.BigIntegerField(db_column='DeleterUserId', blank=True, null=True)
    deletiontime = models.DateTimeField(db_column='DeletionTime', blank=True, null=True)
    lastmodificationtime = models.DateTimeField(db_column='LastModificationTime', blank=True, null=True)
    lastmodifieruserid = models.BigIntegerField(db_column='LastModifierUserId', blank=True, null=True)
    creationtime = models.DateTimeField(db_column='CreationTime')
    creatoruserid = models.BigIntegerField(db_column='CreatorUserId', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Vocabulary'
        verbose_name = 'Vocabulary entry'
        verbose_name_plural = 'Vocabulary entries'
