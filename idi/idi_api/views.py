import json
import logging
from datetime import datetime

from django.core import serializers
from django.db import transaction, connection
from django.utils import timezone

from rest_framework import status
from rest_framework.generics import CreateAPIView, GenericAPIView, DestroyAPIView, UpdateAPIView, RetrieveAPIView, \
    get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.response import Response


from idi_api.models import Metadata, Metadatafiles, MetadataOwnership
from idi_api.utils import AzureBlobStorage, AzureServiceBus, QueryBuilder
from idi_api.serializers import MetadataSerializer, CreateMetadataSerializer, GeographicBoundingBoxSerializer, \
    QueryBuilderSerializer, MultiFilesSerializer, SoftDeleteSerializer, VerboseMetadataSerialized

from django.conf import settings

# hide requests logger
logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(level=logging.WARNING)


class MetadataList(RetrieveAPIView):
    """
    Return the row with the specified ID from the Metadata table.
    """
    queryset = Metadata.objects.all()
    serializer_class = MetadataSerializer
    lookup_field = "id"


class MetadataQueriesRetrieve(GenericAPIView):
    """       
    post:
    Retrieve files based on query
    ---
    
    In order to retrieve files present in the blob storage, this POST API 
    lets the client send a query to filter the metadata. 
    
    The query will be written using a meta-language consisting of 
    possibly-nested lists of query filter descriptions that 
    use the Django notation, which will be then translated to SQL.
    
    Each filter description is a list.  The first element of the 
    list is always the filter operator name. 
    This name is one of either a filter operator, namely `eq` (a synonym for `exact`),
     or the boolean operators `and`, `or`, and `not`.
    
    Primitive query filters have three elements:
    
    `[filteroperator, fieldname, queryarg]`
    
    `filteroperator` must be one of `in`, `range`, `exact`, `iexact`, `lt`, `lte`, `gt`, `gte`.
    
    `fieldname` is the metadata field being queried. `queryarg` 
    is the argument on which the filter is applied.
        
    `and` and `or` query filters are lists that begin with the appropriate 
    operator name, and include subfilters as additional list elements:
        
    `['or', [subfilter1], [subfilter2], ...]`
    
    `['and', [subfilter1], [subfilter2], ...] `
        
    `not` query filters consist of exactly two elements:
        
    `['not', [subfilter]]`
        
    Example queries:
    
    ```json
    ["and", 
        ["in", "identification_resourcetype", ["service", "series"]],
        ["in", "conformity_degree", ["conformant"]]
    ]
    ```
    
    The values for some of the fields have to respect the Enumerators specified here 
    `https://ireactbe-test.azurewebsites.net/api/services/app/Enums/GetEnums`. 
    
    Upon receiving the request, the application will parse and map these field 
    to the corresponding integer values.
    
    The JSON below is an example of a possible qurty to send to the IDI. To try it out
    just copy and paste it in the field box below.
    
    ```json
    {"query": ["and", 
                        ["in", "identification_resourcetype", ["service", "series"]],
                        ["in", "conformity_degree", ["conformant"]]
                    ]}
    ```
    
    At the moment spatial operations are not available on the geographic bounding boxes. 
    Normal queries can still be performed on the four fields that define the bounding boxes 
    present in the `IrGeographicBoundingBoxes` table. In the case a single binary file comprises 
    more than one bounding box the query will match independently each bounding box with the same filter.
    ---
    
    """

    serializer_class = QueryBuilderSerializer
    parser_classes = (JSONParser, )
    log_path = settings.IDI_API_LOG_PATH
    logging.basicConfig(filename=log_path,
                        filemode='a',
                        format='[%(asctime)s] [%(name)s] (%(levelname)s) %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S',
                        level=logging.DEBUG)

    def post(self, request):
        logger = logging.getLogger('idi_get_api')
        logger.info("Request: {}".format(request.data))

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        query = serializer.data['query']
        builder = QueryBuilder()
        limit_q, orderby_q, q = builder.build_query_filter_from_spec(query)

        logger.debug("Query: {}".format(q))

        try:
            if limit_q is not None and orderby_q is not None:
                result = Metadata.objects.only('metadatafileuri', 'id').order_by(orderby_q).filter(q)[:limit_q]
            elif limit_q is not None:
                result = Metadata.objects.only('metadatafileuri', 'id').filter(q)[:limit_q]
            elif orderby_q is not None:
                result = Metadata.objects.only('metadatafileuri', 'id').order_by(orderby_q).filter(q)
            else:
                result = Metadata.objects.only('metadatafileuri', 'id').filter(q)
        except Exception as e:
            return Response(data="The query to the database raise the following error: {}".format(e),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        uris = [[u.metadatafileuri] for u in result]
        ids = [u.id for u in result]

        response = list()
        for k, u in enumerate(uris):
            try:
                result = Metadatafiles.objects.only('filename').filter(metadataid=ids[k])
            except Exception as e:
                return Response(data="The query to the database raise the following error: {}".format(e),
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            # get the default uri if n.uri is None
            filenames = [(n.uri or settings.AZURE_BLOB_STORAGE) + n.filename for n in result]
            if len(filenames) > 0:
                response.append(u + filenames)  # append list
            else:
                response.append(u)

        if len(uris) == 0:
            return Response("Empty QuerySet")
        else:
            return Response(response)


class MetadataQueriesCreate(CreateAPIView):
    """       
    post:
    Add new metadata/binary file
       
    ---
       
    A proper POST request to upload a binary file will have to include both a single binary file and all the required metadata fields. 
        
    For the moment the swagger interface does not allow to make requests uploading binary files, this it is not possible to test
    this api from here. Refer to the README or the provided Python client for a complete example.
    
    **NOTE**: 
    
    - The keys of the `metadata` dictionary are mapped one-to-one with the column names of the `IrMetadata` table, and the keys of the geographic bounding boxes dictionary are mapped one-to-one with the `IrGeographicBoundingBoxes` table. To easily see all the fields accepted just go to `http://idi-ireact.azurewebsites.net/` where you can find more details about the interaction with the APIs.
    - It is important to include the `geographic_bounding_boxes` as in the example, so as a string with the backslashed double quotes, otherwise the request will not be parsed correctly.
    - The key of the `files` dictionary will be the actual name used by the blob storage to store the file, so set it accordingly.
    - If there is any parsing/value error in the request, the IDI will respond with a message indicating what is the error.
    
    """
    # queryset = Irmetadata.objects.all()
    serializer_class = CreateMetadataSerializer
    log_path = settings.IDI_API_LOG_PATH
    logging.basicConfig(filename=log_path,
                        filemode='a',
                        format='[%(asctime)s] [%(name)s] (%(levelname)s) %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S',
                        level=logging.DEBUG)

    def post(self, request, *args, **kwargs):
        logger = logging.getLogger('idi_post_api')
        # First serialize metadata and bounding boxes to check that the
        # data sent by the client is valid

        # =============================================================
        # Validate data and create JSON objects
        # =============================================================

        logger.info("Request: {}".format(request.data))
        try:
            metadata_serializer = self.get_serializer(data=request.data)
        except Exception as e:
            return Response(data=str(e), status=status.HTTP_503_SERVICE_UNAVAILABLE)
        # check data, if there are any problems raise exception
        metadata_serializer.is_valid(raise_exception=True)
        logger.debug("Valid metadata")

        # create metadata object, easier to access data this way
        metadata_ob = Metadata(**metadata_serializer.validated_data)

        # retrieve and validate the bounding boxes
        if 'geographic_bounding_boxes' not in request.data:
            return Response(data="Provide geographic_bounding_boxes key in the request dictionary",
                            status=status.HTTP_400_BAD_REQUEST)
        try:
            b = json.loads(request.data['geographic_bounding_boxes'])
        except json.decoder.JSONDecodeError as e:
            return Response(data="Geographic_bounding_boxes JSONDecodeError: " + str(e),
                            status=status.HTTP_400_BAD_REQUEST)

        bounding_boxes = []
        for _b in b:
            s = GeographicBoundingBoxSerializer(data=_b)
            s.is_valid(raise_exception=True)  # check data, if not valid raise exception
            # bounding_boxes.append(Geographicboundingboxes(**s.validated_data))
            bounding_boxes.append(s.data)
        logger.debug("Valid Bounding Boxes")

        # now convert model objects to JSON (Needed when we have to upload them to ServiceBus)
        metadata_json = json.loads(serializers.serialize('json', [metadata_ob]))[0]['fields']
        # take all the bounding boxes and serialize to json
        bounding_boxes_json = json.dumps(bounding_boxes)
        metadata_json['geographic_bounding_boxes'] = bounding_boxes_json

        # =============================================================
        # In case there are multiple binary file in the request check
        # that all needed fields to create the file names were filled
        # =============================================================

        leadtimes = None
        visualization_order = None
        filenames = None
        timespans = None

        # check that we have at least one binary file in the request
        keys = list(request.FILES.keys())
        if len(keys) < 1:
            return Response("Provide at least one binary file.", status=status.HTTP_400_BAD_REQUEST)

        # validate leadtimes and visualization_order (for multiple binary files)
        multifiles_serializer = MultiFilesSerializer(data=request.data)
        multifiles_serializer.is_valid(raise_exception=True)

        if len(keys) > 1:
            # either the field was not present in the request.data dictionary or it was present and has been validated
            if 'leadtimes' not in multifiles_serializer.validated_data or \
                            len(multifiles_serializer.validated_data['leadtimes']) != len(keys):
                return Response(data={"leadtimes": "must provide a valid list of timestamps for multiple binary files"},
                                status=status.HTTP_400_BAD_REQUEST)
            # same thing for viz_order
            if 'visualization_order' not in multifiles_serializer.validated_data or \
                    len(multifiles_serializer.validated_data['visualization_order']) != len(keys):
                return Response(data={"visualization_order": "must provide a valid list for multiple binary files"},
                                status=status.HTTP_400_BAD_REQUEST)
            if 'timespans' not in multifiles_serializer.validated_data or \
                    len(multifiles_serializer.validated_data['timespans']) != len(keys):
                return Response(data={"timespans": "must provide a valid list for multiple binary files"},
                                status=status.HTTP_400_BAD_REQUEST)
            if 'filenames' not in multifiles_serializer.validated_data or \
                    len(multifiles_serializer.validated_data['filenames']) != len(keys):
                return Response(data={"filenames": "must provide a valid list for multiple binary files"},
                                status=status.HTTP_400_BAD_REQUEST)

            leadtimes = multifiles_serializer.validated_data['leadtimes']
            visualization_order = multifiles_serializer.validated_data['visualization_order']
            timespans = multifiles_serializer.validated_data['timespans']
            filenames = multifiles_serializer.validated_data['filenames']

            # check that the filenames provided match the ones in the FILEs dict
            for _f in filenames:
                if _f not in request.FILES.keys():
                    return Response(data={"filenames": "Metadata filenames must match binary filenames"},
                                    status=status.HTTP_400_BAD_REQUEST)
        else:  # len(keys)=1
            if 'leadtimes' in multifiles_serializer.validated_data and \
                    len(multifiles_serializer.validated_data['leadtimes']) > 1:
                return Response(data={"leadtimes": "Must provide a single leadtime when uploading a single file"},
                                status=status.HTTP_400_BAD_REQUEST)
            if 'leatimes' not in multifiles_serializer.validated_data:
                leadtimes = [metadata_ob.temporalreference_end.strftime("%Y%m%dT%H%M%S")]
            else:
                leadtimes = multifiles_serializer.validated_data['leadtimes']

            if 'timespans' in multifiles_serializer.validated_data and \
                    len(multifiles_serializer.validated_data['timespans']) > 1:
                return Response(data={"timespans": "Must provide a single timespans when uploading a single file"},
                                status=status.HTTP_400_BAD_REQUEST)
            if 'timespans' not in multifiles_serializer.validated_data:
                delta = metadata_ob.temporalreference_end - metadata_ob.temporalreference_start
                timespans = [delta.seconds / 3600]
            else:
                timespans = multifiles_serializer.validated_data['timespans']
            # since there is just one file we take that key
            filenames = list(request.FILES.keys())

        # RULE: if TemporalReference_Start is equal to TemporalReference_End, then the leadtimes have
        #       all to be equal to TemporalReference_Start/End
        tr_start = metadata_ob.temporalreference_start.strftime("%Y%m%dT%H%M%S")
        tr_end = metadata_ob.temporalreference_end.strftime("%Y%m%dT%H%M%S")
        if tr_start == tr_end:
            leadtimes = [tr_start for _ in range(0, len(keys))]
            timespans = [0 for _ in range(0, len(keys))]
        # else:
        #     if any([x == 0 or x == "" for x in timespans]):  # no timestamp can be zero
        #         return Response(
        #             data={"timespans": "Must provide a timespand != 0"},
        #             status=status.HTTP_400_BAD_REQUEST
        #         )

        # Check that the leadtimes are between TRef_Start and TRef_End
        if any(map(lambda x: x < tr_start or x > tr_end, leadtimes)):
            return Response(
                data={"leadtimes": "Leadtimes must be between TemporalReference_Start and TemporalReference_End"},
                status=status.HTTP_400_BAD_REQUEST)

        # =============================================================
        # create the file names of the binaries
        # =============================================================

        # in case we have just one binary file we pass leadtimes=None to the
        # file names generator function, so it will just return the basename
        basename, filenames = self.create_filenames(ireacttask=request.data['ireacttask'],
                                                    # take this from request.data because we need the string,
                                                    # not the integer enum present in metadata_ob
                                                    creationdatetime=metadata_ob.creationtime.strftime("%Y%m%dT%H%M%S"),
                                                    filenames=filenames,
                                                    leadtimes=leadtimes
                                                    )

        # =============================================================
        # load the binary files and the json objects (with the metadata)
        # to the blob storage
        # =============================================================

        blob_storage = AzureBlobStorage()
        # first check that these filenames are not already present in the blob storage
        exist = blob_storage.exists('idi', filenames[1])
        if any(exist):
            # get the first conflicting name
            conflict_name = filenames[1][[i for i, x in enumerate(exist) if x][0]]
            return Response(
                data="A file with name {} already exists in the blob storage".format(conflict_name),
                status=status.HTTP_400_BAD_REQUEST)
        # upload the binaries to the blob storage
        binary_files_urls, excp = self.upload_files_to_blob_storage(blob_storage, request.FILES, filenames)
        if binary_files_urls is None and excp is not None:
            return Response(
                data=str(excp),
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        if binary_files_urls is None:
            return Response(
                data="Something bad happened while uploading a binary file to the Blob Storage.",
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        json_url, excp = self.upload_json_to_blob_storage(blob_storage, metadata_json, basename)
        if excp is not None:
            return Response(
                data=str(excp),
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        if json_url is None:
            self.delete_all_blobs(blob_storage, container='idi', names=filenames[1])
            return Response(
                data="Something bad happened while uploading the json file to the Blob Storage",
                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # =============================================================
        # save metadata and bounding boxes to db
        # =============================================================

        setattr(metadata_ob, 'identification_uri', str(binary_files_urls))
        setattr(metadata_ob, 'metadatafileuri', json_url)

        # build query to create bounding boxes polygon
        # function: bb dictionary to polygon
        polygon = lambda x: "(({} {}, {} {}, {} {}, {} {}, {} {}))".format(x["east"], x["south"],
                                                                  x["east"], x["north"],
                                                                  x["west"], x["north"],
                                                                  x["west"], x["south"],
                                                                  x["east"], x["south"])
        # join all polygons (from polygon function) into single string
        multipolygon = "MULTIPOLYGON({})".format(",".join(map(polygon, bounding_boxes)))
        try:
            # we make changes to the db atomically. In case something breaks,
            # the transaction reverts the changes and we delete the binary file
            # from the data lake.
            with transaction.atomic():
                metadata_ob.save()
                MetadataOwnership.objects.create(owner=request.user, metadata=metadata_ob)
                # get the primary key
                with connection.cursor() as cursor:
                    cursor.execute("UPDATE Metadata SET GeographicBoundingBoxes = "
                                   "geography::STGeomFromText(%s, 4326).MakeValid().STConvexHull()"
                                   "WHERE Id=%s", [multipolygon, metadata_ob.pk])

                # use the created id as foreign key for the geo bounding boxes
                # for box in bounding_boxes:
                #     setattr(box, 'isdeleted', False)
                #     setattr(box, 'metadataid', metadata_ob)
                #     box.save()
                # create and save IrMetadataFiles objects, one for each file
                if len(filenames[1]) == 1:
                    Metadatafiles(filename=filenames[1][0],
                                  metadataid=metadata_ob,
                                  leadtime=datetime.strptime(leadtimes[0], "%Y%m%dT%H%M%S"),
                                  timespan=timespans[0],
                                  uri=settings.AZURE_BLOB_STORAGE).save()
                else:
                    for _filename, _leadtime, _viz_order, _timespan in zip(filenames[1], leadtimes, visualization_order, timespans):
                        Metadatafiles(filename=_filename,
                                      leadtime=datetime.strptime(_leadtime, "%Y%m%dT%H%M%S"),
                                      visualizationorder=_viz_order,
                                      timespan=_timespan,
                                      metadataid=metadata_ob,
                                      uri=settings.AZURE_BLOB_STORAGE).save()
        except Exception as e:
            # delete uploaded files
            self.delete_all_blobs(blob_storage, container='idi', names=filenames[1])
            return Response(data='An error has occurred while inserting new data into the db. Rollback. {}'.format(e),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # =============================================================
        # post a message with the metadata to the ServiceBus Topic
        # =============================================================
        res = self.send_service_bus_message(api='CREATE', metadata=metadata_ob, bounding_boxes=bounding_boxes_json)
        if res is None:
            return Response(data='Metadata and binary file were loaded correctly but there was an error '
                                 'while pushing a message to ServiceBus.',
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # return successful response to client (we send back the message sent to the newexternaldata topic)
        return Response(data=json.dumps(res), status=status.HTTP_200_OK)

    def create_filenames(self, ireacttask, creationdatetime, filenames, leadtimes=None):
        """
        Create the file names for the binary files according to specification
        :param ireacttask: ireacttask of the metadata
        :param creationdatetime: creation time in string format %Y%m%dT%H%M%S
        :param filenames: the filenames (+ extension) provided by the client
        :param leadtimes: timestamps added to the basename to create unique names
        :return: a couple (basename, filenames).
        """
        # take extension of first file (it should be the same for every file in the request)
        extension = filenames[0].split('.')[-1]
        basename = "t" + ireacttask + "_" + creationdatetime
        # in case the request has just one file
        if leadtimes is None:
            return basename + "_" + filenames[0], [[filenames[0]], [basename + "_" + filenames[0]]]
        if type(leadtimes) == list:
            # list two lists: first is list of filenames, second is list of extended filenames
            names = list()
            names.append(filenames)
            tmp = list()
            for _l, _f in zip(leadtimes, filenames):
                tmp.append(basename + "_" + _l + "_" + _f)
            names.append(tmp)
            return basename + "." + extension, names

    def delete_all_blobs(self, blob_storage, container, names):
        for _name in names:
            blob_storage.delete_blob(container, _name)

    def upload_files_to_blob_storage(self, blob_storage, files, filenames):
        """
        Upload the binaries to the BlobStorage.
        :param blob_storage: an instance of AzureBlobStorage
        :param files: a dictionary of binary files (request.FILES)
        :param filenames: a list of file names, 1:1 mapped with `files` parameter
        :returns urls: a list of URLs pointing to the uploaded binaries
        """
        # save binary files to blob storage
        urls = list()
        for _k, _filename in zip(filenames[0], filenames[1]):
            binary_file = files[_k]
            binary_file_url, excp = blob_storage.load_blob_from_stream(_filename, binary_file)
            if excp is not None:
                return None, excp
            urls.append(binary_file_url)
            if binary_file_url is None:
                # If we get here something bad happened.
                return None, None
        return urls, None

    def upload_json_to_blob_storage(self, blob_storage, metadata_json, json_name):
        """
        Upload a JSON file with all the metadata to the Blob Storage
        The name of the file is the base name of the binary files (without the lead times)
        :param blob_storage: an instance of AzureBlobStorage
        :param metadata_json: A JSON object containing all the metadata
        :param json_name: the basename of the binary files
        :return: URL of the uploaded JSON file
        """
        vs = VerboseMetadataSerialized(data=metadata_json)
        vs.is_valid(raise_exception=True)
        metadata_json = json.loads(serializers.serialize("json", [Metadata(**vs.validated_data)]))[0]['fields']
        # to bytes
        json_bytes = bytes(json.dumps(metadata_json), 'utf-8')
        # load to blob storage
        json_file_url, excp = blob_storage.load_blob_from_bytes(json_name + ".json", json_bytes)
        return json_file_url, excp

    @staticmethod
    def send_service_bus_message(api, metadata, bounding_boxes=None):
        """
        Prepare a message for the serivce bus topic. The message has the following fields:
            - metadataid
            - ireacttask (which is the queryable paramter)
            - bounding boxes
            - date of publication
            - ...
        :param api: type of operation
        :param metadata_ob:  a `IrMetadata` object with all the validated metadata
        :param bounding_boxes: a JSON object with all the bounding boxes data
        :return message
        """

        if api not in ['CREATE', 'DELETE', 'UPDATE']:
            # default to CREATE api
            api = 'CREATE'

        message = dict()
        message['action'] = api
        message['metadataid'] = metadata.pk
        message['ireacttask'] = metadata.ireacttask
        message['temporalreference_dateofcreation'] = metadata.temporalreference_dateofcreation.strftime(
            "%Y%m%dT%H%M%S")
        if metadata.temporalreference_start is None:
            message['start'] = 'None'
        else:
            message['start'] = metadata.temporalreference_start.strftime("%Y%m%dT%H%M%S")
        if metadata.temporalreference_end is None:
            message['end'] = 'None'
        else:
            message['end'] = metadata.temporalreference_end.strftime("%Y%m%dT%H%M%S")

        if api == 'CREATE':
            message['geographicBoundingBoxes'] = bounding_boxes

        try:
            # Send the message to the IDI Topic
            service_bus = AzureServiceBus()
            # custom_properties is needed for subscription's filters
            service_bus.send_message_to_topic(json.dumps(message),
                                              custom_properties={'ireacttask': message['ireacttask']},)
        except Exception as e:
            # there has been some error in pushing the message to service bus
            print(e)
            return None
        return message


class MetadataSoftDelete(DestroyAPIView):
    """       
    delete:
    Soft delete on metadata records

    ---

    A proper POST request to **soft** delete should include just the `metadataid` of the record to delete.
    
    All the binary files present in the data lake associated to the soft deleted records are left untouched.
    """
    serializer_class = SoftDeleteSerializer
    log_path = settings.IDI_API_LOG_PATH
    logging.basicConfig(filename=log_path,
                        filemode='a',
                        format='[%(asctime)s] [%(name)s] (%(levelname)s) %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S',
                        level=logging.DEBUG)

    def delete(self, request, *args, **kwargs):
        logger = logging.getLogger('idi_soft_delete_api')
        logger.info("Request: {}".format(request.data))

        metadataid = kwargs['id']


        # get the metadata of the user
        # making sure the owner is the requesting user and that it is not deleted yet
        metadata = get_object_or_404(
            Metadata.objects.of(request.user, isdeleted=False),
            pk=metadataid
        )

        # =============================================================
        # Perform soft delete
        # =============================================================
        try:
            # we make changes to the db atomically. In case something breaks,
            # the transaction reverts the changes
            with transaction.atomic():
                # perform the soft delete, i.e. update the 'isdeleted' field from the record.
                metadata.isdeleted = True
                metadata.deletiontime = datetime.now(timezone.utc).isoformat()

                metadata.save(update_fields=['isdeleted', 'deletiontime'])

        except Exception as e:
            return Response(data='An error has occurred while performing the soft delete. Rollback. {}'.format(e),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        res = MetadataQueriesCreate.send_service_bus_message(api='DELETE', metadata=metadata)
        if res is None:
            return Response(data='Soft delete operation completed successfully but there was an error '
                                 'while pushing a message to ServiceBus.',
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(data=json.dumps(res), status=status.HTTP_200_OK)


class MetadataUpdate(UpdateAPIView):
    """       
        put:
        Update metadata with new values
        
        patch:
        Similar to PUT endpoint, just use PUT     
        ---

        A PUT request to update a record of the metadata table. 
        """

    serializer_class = MetadataSerializer
    queryset = Metadata.objects.all()
    lookup_field = 'id'

    def get_queryset(self):
        return Metadata.objects.of(self.request.user, isdeleted=False)

    def update(self, request, *args, **kwargs):
        # get data currently stored in the database
        # get_object automatically gets the row with the id specified in the request URL
        row_data = self.get_object()
        metadata_serializer = self.get_serializer(row_data, data=request.data, partial=True)
        # check data, if there are any problems raise exception
        metadata_serializer.is_valid(raise_exception=True)

        metadata = Metadata(**metadata_serializer.validated_data)
        metadata.id = row_data.id

        try:
            # we make changes to the db atomically. In case something breaks,
            # the transaction reverts the changes
            with transaction.atomic():
                metadata.save(update_fields=list(request.data.keys()))
                # self.perform_update(metadata_serializer)
        except Exception as e:
            return Response(data='An error has occurred while updating the data on the database. Rollback. {}'.format(e),
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # get updated metadata
        new_metadata = self.get_object()
        res = MetadataQueriesCreate.send_service_bus_message(api='UPDATE', metadata=new_metadata)
        if res is None:
            return Response(data='Soft delete operation completed successfully but there was an error '
                                 'while pushing a message to ServiceBus.',
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(data=json.dumps(res), status=status.HTTP_200_OK)
