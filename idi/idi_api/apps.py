from django.apps import AppConfig


class IdiApiConfig(AppConfig):
    name = 'idi_api'
