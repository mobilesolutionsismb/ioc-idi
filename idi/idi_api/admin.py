from django.contrib import admin
from .models import Metadata, Vocabulary


class MetadataAdmin(admin.ModelAdmin):
    list_display = ('identification_resourcetitle', 'identification_resourcetype')
    list_filter = ('creationtime', 'isdeleted', 'identification_resourcelanguage')
    search_fields = ['identification_resourcetitle',  'identification_resourcetype', 'identification_resourcelanguage']
    actions = None
    inlines = []

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class VocabularyAdmin(admin.ModelAdmin):
    list_display = ('originatingcontrolledvocabulary', 'keyword', 'creationtime')
    list_filter = ('creationtime', 'isdeleted')
    search_fields = ['keyword',  'description']
    actions = None

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Metadata, MetadataAdmin)
admin.site.register(Vocabulary, VocabularyAdmin)
