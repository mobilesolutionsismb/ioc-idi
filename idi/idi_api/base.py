from sql_server.pyodbc.base import DatabaseWrapper as DBW


class DatabaseWrapper(DBW):
    def get_new_connection(self, conn_params):
        conn = super().get_new_connection(conn_params)

        if conn is not None:
            conn.add_output_converter(-151, lambda _: "Binary format not supported.")

        return conn
