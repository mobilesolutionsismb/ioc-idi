from django.conf.urls import url
from idi_api import views


urlpatterns = [
    url(r'^metadata/(?P<id>\d+)/$', views.MetadataList.as_view()),
    url(r'^metadataquery/create/$', views.MetadataQueriesCreate.as_view(), name='metadata-create'),
    url(r'^metadataquery/retrieve/$', views.MetadataQueriesRetrieve.as_view(), name='metadata-retrieve'),
    url(r'^metadataquery/softdelete/(?P<id>\d+)/$', views.MetadataSoftDelete.as_view(), name='metadata-delete'),
    url(r'^metadataquery/update/(?P<id>\d+)/$', views.MetadataUpdate.as_view(), name='metadata-update')
]
