import json
from django.db.models.query import Q
import requests
from azure.storage.blob import BlockBlobService
from azure.storage.blob import PublicAccess
from azure.servicebus import ServiceBusService, Message
from rest_framework.exceptions import ValidationError

from django.conf import settings


class AzureBlobStorage:
    """
    Handy class used to handle the basic operations to interact with
    the Azure Data Lake.
    """

    def __init__(self):
        # create a BLockBlobService object to connect to the blob storage
        self.account_name = settings.AZURE_BLOB_STORAGE_ACCOUNT_NAME
        self.account_key = settings.AZURE_BLOB_STORAGE_ACCOUNT_KEY
        self.container = settings.AZURE_BLOB_STORAGE_CONTAINER
        self.blob_url_template = "https://{}.blob.core.windows.net/{}/{}"  # (account name, container, blob_name)
        try:
            self.block_blob_service = BlockBlobService(account_name=self.account_name, account_key=self.account_key)
        except Exception as e:
            print(e)

    def create_container(self, container_name, public_access=False):
        try:
            result = self.block_blob_service.create_container(container_name=container_name)
        except Exception as e:
            print(e)
            return False
        if not result:
            raise Exception("Container already exists.")
        if public_access:
            try:
                self.block_blob_service.set_container_acl(
                    container_name=container_name, public_access=PublicAccess.Container)
            except Exception as e:
                print(e)
                return False
        return True

    def load_blob(self, func, blob_name, file):
        # First check if blob already exists
        if self.block_blob_service.exists(container_name=self.container, blob_name=blob_name):
            return None, "Blob already exists - blob_name: " + blob_name
        try:
            result = func(
                self.container,
                blob_name,
                file
                # content_settings=ContentSettings(content_type='image/png')
            )
        except Exception as e:
            print(e)
            return None, e
        # return the url of the blob
        return self.blob_url_template.format(self.account_name, self.container, blob_name), None

    def load_blob_from_stream(self, blob_name, file):
        return self.load_blob(self.block_blob_service.create_blob_from_stream, blob_name, file)

    def load_blob_from_bytes(self, blob_name, file):
        return self.load_blob(self.block_blob_service.create_blob_from_bytes, blob_name, file)

    def get_blob(self, blob_name, file_path):
        # returns a blob object
        try:
            blob = self.block_blob_service.get_blob_to_path(container_name=self.container, blob_name=blob_name,
                                                            file_path=file_path)
        except Exception as e:
            print(e)
            return None
        return blob

    def delete_blob(self, container, blob_name):
        self.block_blob_service.delete_blob(container_name=container, blob_name=blob_name)

    def list_blobs(self, container):
        blobs = []
        try:
            generator = self.block_blob_service.list_blobs(container)
        except Exception as e:
            print(e)
            return None
        for blob in generator:
            blobs.append(blob.name)
        return blobs

    def exists(self, container, names):
        # return an existential boolean for each name
        return [self.block_blob_service.exists(container, _name) for _name in names]


class AzureServiceBus:
    def __init__(self):
        self.topic_name = settings.AZURE_SERVICE_BUS_TOPIC_NAME
        self.service_namespace = settings.AZURE_SERVICE_BUS_NAMESPACE,
        self.shared_access_key_name = settings.AZURE_SERVICE_BUS_KEY_NAME,
        self.shared_access_key_value = settings.AZURE_SERVICE_BUS_ACCESS_KEY
        # self.bus_service = ServiceBusService(
        #     service_namespace="ireact-dev",
        #     shared_access_key_name="RootManageSharedAccessKey",
        #     shared_access_key_value="//tsxB/acZzy5otVtrJT1jTQoJtP+Vq/3+M+k4KCpfI=")
        self.bus_service = ServiceBusService(
            service_namespace=settings.AZURE_SERVICE_BUS_NAMESPACE,
            shared_access_key_name=settings.AZURE_SERVICE_BUS_KEY_NAME,
            shared_access_key_value=settings.AZURE_SERVICE_BUS_ACCESS_KEY)

    def create_topic(self, topic):
        new_topic_name = topic
        self.bus_service.create_topic(new_topic_name)
        self.bus_service.create_subscription(new_topic_name, 'AllMessages')

    def send_message_to_topic(self, message, custom_properties):
        msg = Message(message, custom_properties=custom_properties)
        self.bus_service.send_topic_message(self.topic_name, msg)

    def __get_message_from_topic(self, topic='idi'):
        """
        This method should not be used by the idi 
        """
        return self.bus_service.receive_subscription_message(topic, 'AllMessages', peek_lock=False)


class QueryBuilder:

    def __init__(self):
        # retrieve the enumerators
        self.root = True
        self.order_by = None
        self.limit = None
        response = requests.get(settings.IREACT_ENUMERATORS)
        self.enumerators = json.loads(response.text)['result']
        self.name_map = {
            "identification_resourcetype": "resourceType",
            "classification_topiccategory": "topicCategory",
            "classification_spatialdataservicetype": "spatialDataServiceType",
            "qualityandvalidity_spatialresolution_measureunit": "measureUnit",
            "conformity_degree": "degree",
            "responsibleorganization_responsiblepartyrole": "responsiblePartyRole"
        }
        self.geo_fields = ['areaofinterest_north', 'areaofinterest_south',
                           'areaofinterest_east', 'areaofinterest_west']

    def build_query_filter_from_spec(self, spec, field_mapping=None):
        """ 
        Assemble a django "Q" query filter object from a specification that consists 
        of a possibly-nested list of query filter descriptions.  These descriptions
        themselves specify Django primitive query filters, along with boolean 
        "and", "or", and "not" operators.  This format can be serialized and 
        deserialized, allowing django queries to be composed client-side and
        sent across the wire using JSON.
    
        Each filter description is a list.  The first element of the list is always 
        the filter operator name. This name is one of either django's filter 
        operators, "eq" (a synonym for "exact"), or the boolean operators
        "and", "or", and "not".
    
        Primitive query filters have three elements:
    
        [filteroperator, fieldname, queryarg]
    
        "filteroperator" is a string name like "in", "range", "icontains", etc. 
        "fieldname" is the django field being queried.  Any name that django
        accepts is allowed, including references to fields in foreign keys
        using the "__" syntax described in the django API reference. 
        "queryarg" is the argument you'd pass to the `filter()` method in
        the Django database API.
    
        "and" and "or" query filters are lists that begin with the appropriate 
        operator name, and include subfilters as additional list elements:
    
        ['or', [subfilter], ...]
        ['and', [subfilter], ...] 
    
        "not" query filters consist of exactly two elements:
    
        ['not', [subfilter]]
    
        As a special case, the empty list "[]" or None return all elements.
    
        If field_mapping is specified, the field name provided in the spec
        is looked up in the field_mapping dictionary.  If there's a match,
        the result is subsitituted. Otherwise, the field name is used unchanged
        to form the query. This feature allows client-side programs to use
        "nice" names that can be mapped to more complex django names. If
        you decide to use this feature, you'll probably want to do a similar
        mapping on the field names being returned to the client.
    
        This function returns a Q object that can be used anywhere you'd like
        in the django query machinery.
    
        This function raises ValueError in case the query is malformed, or
        perhaps other errors from the underlying DB code.
    
        Example queries:
    
        ['limit', N, ...]
        ['orderby', 'colum_name', ...]
        ['limit', N, ['orderby', 'colum_name', ...]]
        ['and', ['contains', 'name', 'Django'], ['range', 'apps', [1, 4]]]
        ['not', ['in', 'tags', ['colors', 'shapes', 'animals']]]
        ['or', ['eq', 'id', 2], ['icontains', 'city', 'Boston']]
    
        """
        resultq = None
        if spec is None or len(spec) == 0:
            return Q()
        cmd = spec[0]

        # -----------------------------------

        if self.root and cmd == 'limit':
            if len(spec) < 2:
                raise ValidationError('"limit" keyword must have two arguments. Example: ["limit", N, query]')
            self.limit = spec[1]  # N of limit keyword
            if self.limit < 1:
                raise ValidationError('"limit" must have a value greater than 0')
            spec = spec[2]
            cmd = spec[0]

        if self.root and cmd == 'orderby':
            if len(spec) < 2:
                raise ValidationError(
                    '"orderby" keyword must have two arguments. Example: ["orderby", "colum_name", ...]')
            self.order_by = spec[1]  # order_by column name
            spec = spec[2]
            cmd = spec[0]

        if self.root and cmd == 'orderby_desc':
            if len(spec) < 2:
                raise ValidationError(
                    '"orderby_desc" keyword must have two arguments. Example: ["orderby_desc", "column_name", ...]')
            self.order_by = "-" + spec[1]  # order by column name in descending order
            spec = spec[2]
            cmd = spec[0]

        self.root = False

        # -----------------------------------

        if cmd == 'and' or cmd == 'or':
            # ["or",  [filter],[filter],[filter],...]
            # ["and", [filter],[filter],[filter],...]
            if len(spec) < 2:
                raise ValidationError('"and" or "or" filters must have at least one subfilter')

            if cmd == 'and':
                qop = Q.AND
            else:
                qop = Q.OR

            for arg in spec[1:]:
                q = self.build_query_filter_from_spec(arg)[2]
                if q is not None:
                    if resultq is None:
                        resultq = q
                    else:
                        # resultq = qop(resultq, q)
                        resultq = resultq.add(q, qop)

        elif cmd == 'not':
            # ["not", [query]]
            if len(spec) != 2:
                raise ValidationError('"not" filters must have exactly one subfilter')
            q = self.build_query_filter_from_spec(spec[1])
            if q is not None:
                # resultq = QNot(q)
                resultq = ~q

        else:
            # some other query, will be validated in the query machinery
            # ["cmd", "fieldname", "arg"]

            # https://docs.djangoproject.com/en/1.10/ref/models/querysets/#field-lookups
            # have a look at lookups like 'year', 'month', 'week' etc.. to match exactly a part of the date
            allowed_commands = ['equal', 'exact', 'iexact', 'in', 'range', 'lt', 'lte', 'gt', 'gte']
            if cmd not in allowed_commands:
                raise ValidationError("A command must be one of {}".format(allowed_commands))

            # provide an intuitive alias for exact field equality
            if cmd == 'equal':
                cmd = 'exact'

            if len(spec) != 3:
                raise ValidationError('Primitive filters must have two arguments (fieldname and query arg)')

            field_name = spec[1]
            if field_mapping:
                # see if the mapping contains an entry for the field_name
                # (for example, if you're mapping an external database name
                # to an internal django one).  If not, use the existing name.
                field_name = field_mapping.get(field_name, field_name)

            # map the query values in case the query contains filters
            # on the enumerators
            if field_name in list(self.name_map.keys()):
                spec[2] = self.map_enumerator(field_name, spec[2])

            # TODO: manage here case for bounding boxes (do we need spatial lookups)
            # for now we just map the bounding boxes names to the table columns to allow
            # simple operations on the integers that define the bounding box
            if field_name in self.geo_fields:
                # access field referenced by foreign key. <foreign_table>__<foreign_field>
                field_name = 'irgeographicboundingboxes__' + field_name
            # Spatial lookups
            # https://docs.djangoproject.com/en/1.10/ref/contrib/gis/geoquerysets/#spatial-lookups
            # https://docs.djangoproject.com/en/1.10/ref/contrib/gis/db-api/#spatial-lookup-compatibility
            # https://docs.djangoproject.com/en/1.10/ref/contrib/gis/db-api/
            # example of Q query with foreign key:
            # Q(irgeographicboundingboxes__areaofinterest_south__exact=2)

            kwname = str("%s%s%s" % (field_name, '__', cmd))
            kwdict = {kwname: spec[2]}
            resultq = Q(**kwdict)

        return self.limit, self.order_by, resultq

    def map_enumerator(self, field_name, values):
        enum = self.get_enum_name(field_name)
        mapped = list()
        for v in values:
            if v not in self.enumerators[enum]:
                raise ValidationError("{} must be one of: {}".format(enum, values))
            mapped .append(self.enumerators[enum][v])
        return mapped

    def get_enum_name(self, field_name):
        if field_name not in list(self.name_map.keys()):
            raise ValidationError("{} not recognized.".format(field_name))
        return self.name_map[field_name]
