import requests
import json
import sys
import os
import argparse

IDI_ENDPOINT_LOCAL = 'http://127.0.0.1:8000/'
IDI_ENDPOINT_DEV = 'http://idi-dev.ireact.cloud/'
IDI_ENDPOINT_TEST = 'http://idi-test.ireact.cloud/'
IDI_ENDPOINT_PROD = 'http://idi.ireact.cloud/'

class Updater:

    def __init__(self, url, user, pwd, metadata_id, metadata_path):
        self.user = user
        self.pwd = pwd
        self.metadata_id = metadata_id
        self.metadata_path = metadata_path
        self.metadata = None

        self.url = url
        self.url_auth = self.url + "api-token-auth/"
        self.url_create = "{}apimetadataquery/update/{}/".format(self.url, metadata_id)

    @property
    def local_metadata(self):
        return {
            "identification_resourcetype": "dataset",
            "classification_topiccategory": "utilitiesCommunication"
        }

    def get_metadata(self):
        if self.metadata_path == 'None':
            return self.local_metadata
        else:
            # read metadata from the specified json file
            with open(self.metadata_path) as data_file:
                metadata = json.load(data_file)
            return metadata

    def get_jwt_token(self):
        """
        Acquire the authorization token by logging into the application
        :param user: Username (default 'test')
        :param pwd: Password (default 'test')
        :return: The JWT authorization token
        """
        tk = requests.post(self.url_auth, data={'username': self.user, 'password': self.pwd}).text
        return json.loads(tk)['token']


    def update_metadata(self):
        """
        Sends a 'put' request to the server with the metadata to be updated.
        The application updates the corresponding data in the database
        """
        token = self.get_jwt_token()
        # send the post request
        metadata = self.get_metadata()
        r = requests.put(self.url_create, headers={'Authorization': 'JWT {}'.format(token)}, data=metadata)

        if r.status_code == 200:
            # Print HTTP status code of the response
            print(r.status_code)
            # Print the body of the response
            print(json.loads(r.text))
        else:
            print(r.status_code)
            # Print the body of the response
            print(r.text)


def main(argv):
    # read CLI argument
    parser = argparse.ArgumentParser(description='Update metadata present in the databse.')
    # parser.add_argument('--url', metavar='url', type=str, default='http://idi-ireact.azurewebsites.net/',
    #                     help='url location of the IDI')
    parser.add_argument('--env', default='localhost',
                        help='idi endpoint')
    parser.add_argument('--user', metavar='user', type=str, nargs=1, default='test',
                        help='username to log into the IDI')
    parser.add_argument('--pwd', help='password of the IDI account',
                        type=str, default='test')
    parser.add_argument('--metadata_id', help='ID of the metadata record to be updated',
                        type=int, required=True)
    parser.add_argument('--metadata', help='path to metadata file containing the update json',
                        type=str, default='None')

    args = parser.parse_args(argv)

    if args.metadata != 'None' and not os.path.isdir(args.metadata):
        print("{} folder does not exist".format(args.metadata))

    url = IDI_ENDPOINT_LOCAL  # default value
    if args.env == 'localhost':
        url = IDI_ENDPOINT_LOCAL
    elif args.env == 'dev':
        url = IDI_ENDPOINT_DEV
    elif args.env == 'test':
        url = IDI_ENDPOINT_TEST
    elif args.env == 'prod':
        url = IDI_ENDPOINT_PROD
    else:
        url = args.env

    print("Metadata: {}".format(args.metadata_id))
    updater = Updater(url, args.user, args.pwd, args.metadata_id, args.metadata)
    updater.update_metadata()

if __name__ == "__main__":
    main(sys.argv[1:])
