# in the case you are testing the APIs, this may be useful to delete quickly
# the test files uploaded to the blob storage

from azure.storage.blob import BlockBlobService
from azure.storage.blob import PublicAccess

account_name = ""
account_key = ""
        
block_blob_service = BlockBlobService(account_name=account_name, account_key=account_key)

block_blob_service.delete_blob(container_name='idi', blob_name="test.txt")
block_blob_service.delete_blob(container_name='idi', blob_name="test.txt.json")
