import requests
import json
import sys
import os
import argparse

IDI_ENDPOINT_LOCAL = 'http://127.0.0.1:8000/'
IDI_ENDPOINT_DEV = 'http://idi-dev.ireact.cloud/'
IDI_ENDPOINT_TEST = 'http://idi-test.ireact.cloud/'
IDI_ENDPOINT_PROD = 'http://idi.ireact.cloud/'

class Deleter:

    def __init__(self, url, user, pwd, id):
        self.user = user
        self.pwd = pwd
        self.id = id

        self.url = url
        # self.url = "http://idi-ireact.azurewebsites.net/"
        self.url_auth = self.url + "api-token-auth/"
        self.url_create = self.url + "apimetadataquery/softdelete/"


    def get_jwt_token(self):
        """
        Acquire the authorization token by logging into the application
        :return: The JWT authorization token
        """
        tk = requests.post(self.url_auth, data={'username': self.user, 'password': self.pwd}).text
        return json.loads(tk)['token']


    def post_delete(self):
        """
        Sends a 'put' request to the server with the metadata to be updated.
        The application updates the corresponding data in the database
        :param token: The JWT authentication token
        :param metadataid: The ID of the Metadata record to delete
        """
        # add to the url the id of the metadata record to update
        url_create = self.url + "apimetadataquery/softdelete/" + str(self.id) + "/"
        token = self.get_jwt_token()
        # send the post request
        r = requests.delete(url_create, headers={'Authorization': 'JWT {}'.format(token)})

        if r.status_code == 200:
            # Print HTTP status code of the response
            print(r.status_code)
            # Print the body of the response
            print(json.loads(r.text))
        else:
            print(r.status_code)
            # Print the body of the response
            print(r.text)


def main(argv):
    # read CLI argument
    parser = argparse.ArgumentParser(description='Soft delete file uploaded to the IDI')
    # parser.add_argument('--url', metavar='url', type=str, default='http://idi-ireact.azurewebsites.net/',
    #                     help='url location of the IDI')
    parser.add_argument('--env', default='localhost',
                        help='idi endpoint')
    parser.add_argument('--user', metavar='user', type=str, nargs=1, default='test',
                        help='username to log into the IDI')
    parser.add_argument('--pwd', help='password of the IDI account',
                        type=str, default='test')
    parser.add_argument('--id', help='ID of the metadata to be deleted',
                        type=int, required=True)

    args = parser.parse_args(argv)

    url = IDI_ENDPOINT_LOCAL  # default value
    if args.env == 'localhost':
        url = IDI_ENDPOINT_LOCAL
    elif args.env == 'dev':
        url = IDI_ENDPOINT_DEV
    elif args.env == 'test':
        url = IDI_ENDPOINT_TEST
    elif args.env == 'prod':
        url = IDI_ENDPOINT_PROD
    else:
        url = args.env

    print(f"Soft delete metadata ID: {args.id}")
    deleter = Deleter(url, args.user, args.pwd, args.id)
    deleter.post_delete()

if __name__ == '__main__':
    main(sys.argv[1:])
