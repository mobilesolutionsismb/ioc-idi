import sys
import os
import requests
import json
import argparse

IDI_ENDPOINT_LOCAL = 'http://127.0.0.1:8000/'
IDI_ENDPOINT_DEV = 'http://idi-dev.ireact.cloud/'
IDI_ENDPOINT_TEST = 'http://idi-test.ireact.cloud/'
IDI_ENDPOINT_PROD = 'http://idi.ireact.cloud/'

class Uploader:

    def __init__(self, url, user, pwd, files, metadata_path):
        self.user = user
        self.pwd = pwd
        self.paths = files
        self.token = None
        self.metadata = None
        self.metadata_path = metadata_path

        self.url = url
        # self.url = "http://idi-ireact.azurewebsites.net/"
        self.url_auth = self.url + "api-token-auth/"
        self.url_create = self.url + "apimetadataquery/create/"

    def upload_files(self):
        # acquire an authentication token
        self.token = self.get_jwt_token()
        self.metadata = self.get_metadata()
        # send a binary_file + metadata
        self.post_create()

    def get_metadata(self):
        """
        Dummy example of a metadata JSON dictionary with some dummy values.
        Refer to https://docs.google.com/spreadsheets/d/17wACHqUCSthTXiAfNfLbA-QPc4AeNGRbQlkpcUAyfH0/
         for a complete reference to the metadata fields

        Some fields are mapped to enumerators present here:
            https://ireactbe-test.azurewebsites.net/swagger/ui/index.html#/Enums

        Have a look at the home page of the IDI at http://idi-ireact.azurewebsites.net/

        The provided README provide additional documentation regarding the format of the request
         and the expected syntax.
        """

        # read metadata from the specified json file
        if self.metadata_path is not None:
            with open(self.metadata_path) as data_file:
                metadata = json.load(data_file)
            return metadata

        metadata_required = {
            "classification_topiccategory": "geoscientificInformation",
            "classification_spatialdataservicetype": "view",
            "qualityandvalidity_spatialresolution_measureunit": "m",
            "keyword_originatingcontrolledvocabulary": "gemet",
            # if you specify a vocabulary remember to also add some keywords!!
            "keyword_keywordvalue": "Addresses,Atmospheric conditions",
            "conformity_degree": "conformant",
            "responsibleorganization_responsiblepartyrole": "author",
            # change the ireacttask accordingly
            "ireacttask": 9999,
            "isdeleted": "False",
            "geographic_bounding_boxes": "[{\"areaofinterest_north\":3.5, \"areaofinterest_south\":2, \"areaofinterest_west\":3, \"areaofinterest_east\":4}]",
            # in case there are multiple files in the request (2 files in this case)
            "leadtimes": "[\"20170203T121212\", \"20170203T121212\", \"20170203T121212\"]",
            "timespans": "[1, 2, 3]",
            "visualization_order": "[1, 2, 3]",
            "identification_resourcetitle": "some_title",
            "identification_resourceabstract": "some description of the data",
            "identification_resourcetype": "service",
            "identification_resourcelanguage": "eng",
            "temporalreference_start": "2016-07-01T00:00",
            "temporalreference_end": "2016-07-01T00:00",
            "temporalreference_dateofpublication": "2016-07-01T00:00",
            "temporalreference_dateoflastrevision": "2016-07-01T00:00",
            "temporalreference_dateofcreation": "2016-07-01T00:00",
            "qualityandvalidity_lineage": "string",
            "qualityandvalidity_spatialresolution_latitude": 1,
            "qualityandvalidity_spatialresolution_longitude": 1,
            "qualityandvalidity_spatialresolution_scale": 1000,
            "conformity_specification": "see the spreadsheet for an example",
            "responsibleorganization_responsibleparty_organizationname": "org_name",
            "responsibleorganization_responsibleparty_email": "email",
            "metadataonmetadata_pointofcontact_organizationname": "ISMB",
            "metadataonmetadata_pointofcontact_email": "string",
            "metadataonmetadata_language": "eng",
            "metadataonmetadata_date": "2016-07-01T00:00",
            "acquisitiondate": "2016-07-01T00:00",
            "coordinatesystemreference_code": "http://www.opengis.net/def/crs/EPSG/0/3857",
            "coordinatesystemreference_codespace": "EPSG",
        }

        # optional data that can be added to the required one
        metadata_optional = {
            "identification_uri": "uri",
            "identification_coupledresource": "string",
            "temporalreference_dateofcreation": "2016-07-01T00:00",
            "qualityandvalidity_lineage": "string",
            "constraints_conditionsforaccessanduse": "TBD",
            "constraints_limitationsonpublicaccess": "TBD",
            "metadataonmetadata_date": "date value",
        }

        return metadata_required

    def get_jwt_token(self):
        """
        Acquire the authorization token by logging into the application
        :return: The JWT authorization token
        """
        tk = requests.post(self.url_auth, data={'username': self.user, 'password': self.pwd}).text
        return json.loads(tk)['token']

    def post_create(self):
        """
        Sends a 'create' request to the server with metadata and binary file.
        The application loads the binary file into the data lake and writes
        the metadata into the database.
        """
        # create a dictionary with all the files
        files = dict()
        for p in self.paths:
            files.update({os.path.basename(p): open(p, "rb").read()})
        # send the post request
        print(self.url_create)
        r = requests.post(self.url_create,
                          headers={'Authorization': 'JWT {}'.format(self.token)},
                          data=self.metadata,
                          files=files)

        if r.status_code == 200:
            # Print HTTP status code of the response
            print(r.status_code)
            # Print the body of the response
            print(json.loads(r.text))
        else:
            print(r.status_code)
            # Print the body of the response
            print(r.text)


def main(argv):
    # read CLI argument
    parser = argparse.ArgumentParser(description='Upload files with metadata to the IDI. '
                                                 'If you specify just the path to a dir with --path the script '
                                                 'will upload all the files present in the directory and upload '
                                                 'the metadata present in the metadata.json file')
    # parser.add_argument('--url', metavar='url', type=str, default='http://idi-ireact.azurewebsites.net/',
    #                     help='url location of the IDI')
    parser.add_argument('--env', default='localhost',
                        help='idi endpoint')
    parser.add_argument('--user', metavar='user', type=str, nargs=1, default='test',
                        help='username to log into the IDI')
    parser.add_argument('--pwd', help='password of the IDI account',
                        type=str, default='test')
    parser.add_argument('--path', help='path to directory containing files',
                        type=str, required=True)
    parser.add_argument('--files', help='file names', nargs="*")
    parser.add_argument('--metadata', help='file name of json file metadata')

    args = parser.parse_args(argv)

    if not os.path.isdir(args.path):
        print("{} folder does not exist".format(args.path))

    url = IDI_ENDPOINT_LOCAL  # default value
    if args.env == 'localhost':
        url = IDI_ENDPOINT_LOCAL
    elif args.env == 'dev':
        url = IDI_ENDPOINT_DEV
    elif args.env == 'test':
        url = IDI_ENDPOINT_TEST
    elif args.env == 'prod':
        url = IDI_ENDPOINT_PROD
    else:
        url = args.env

    if args.files is None:  # get all files in directory
        args.files = list(filter(lambda f: not f.startswith('.'), os.listdir(args.path)))
        print(args.files)
        # remove metadata.json from files
        args.files.remove("metadata.json")
    paths = [os.path.join(args.path, f) for f in args.files]
    if args.metadata is not None:
        metadata_file = os.path.join(args.path, args.metadata)
    else:
        metadata_file = os.path.join(args.path, "metadata.json")
    print(metadata_file)
    print(paths)
    uploader = Uploader(url, args.user, args.pwd, paths, metadata_file)
    uploader.upload_files()

if __name__ == "__main__":
    main(sys.argv[1:])
