import requests
import json

ENV = 'dev'  # localhost | dev | test | prod
IDI_ENDPOINT_LOCAL = 'http://127.0.0.1:8000/'
IDI_ENDPOINT_DEV = 'http://idi-dev.ireact.cloud'
IDI_ENDPOINT_TEST = 'http://idi-test.ireact.cloud/'
IDI_ENDPOINT_PROD = 'http://idi.ireact.cloud/'
URL = IDI_ENDPOINT_LOCAL  # default value
if ENV == 'localhost':
    URL = IDI_ENDPOINT_LOCAL
elif ENV == 'dev':
    URL = IDI_ENDPOINT_DEV
elif ENV == 'test':
    URL = IDI_ENDPOINT_TEST
elif ENV == 'prod':
    URL = IDI_ENDPOINT_PROD


def get_query():
    """
    Dummy example of a query to be performed on the metadata to retrieve the URIs 
    of the matching binary files from the data lake.
    
    Have a look at the home page of the IDI at http://idi-ireact.azurewebsites.net/
    
    The provided README provide additional documentation regarding the format of the request
    and the expected syntax.
    """
    query = {"query": ["and",
                     ["in", "identification_resourcetype", ["service", "series"]],
                     ["in", "conformity_degree", ["conformant"]]
                     ]
           }
    return query


def get_jwt_token(user="test", pwd="test"):
    """
    Acquire the authorization token by logging into the application
    :param user: Username (default 'test')
    :param pwd: Password (default 'test')
    :return: The JWT authorization token
    """
    url_auth = URL + "api-token-auth/"
    tk = requests.post(url_auth, data={'username': 'test', 'password': 'test'}).text
    return json.loads(tk)['token']


def post_retrieve(token, query):
    """
    Sends a retrieve request to the IDI. If there are any error the IDI
    will send back a message stating what is wrong.
    :param token: The JWT authorization token
    :param query: The query in JSON format
    """
    url_retrieve = URL + "apimetadataquery/retrieve/"
    # send the retrieve request
    r = requests.post(url_retrieve, headers={'Authorization': 'JWT {}'.format(token)}, json=query)
    # Print HTTP status code of the response
    print(r.status_code)
    # Print the body of the response
    print(r.text)


def main():
    # first we have to acquire an authentication token
    token = get_jwt_token()

    query = get_query()

    # or retrieve some links to binary files sending a query
    post_retrieve(token, query)
