from azure.servicebus import ServiceBusService, Message, Topic, Rule, DEFAULT_RULE_NAME
import json

ENV = 'test'  # dev | test | prod
if ENV == 'dev':
    AZURE_SERVICE_BUS_NAMESPACE = "ireact-dev"
    AZURE_SERVICE_BUS_TOPIC_NAME = ""
    AZURE_SERVICE_BUS_KEY_NAME = ""
    AZURE_SERVICE_BUS_ACCESS_KEY = ""
elif ENV == 'test':
    AZURE_SERVICE_BUS_NAMESPACE = "ireact-test"
    AZURE_SERVICE_BUS_TOPIC_NAME = ""
    AZURE_SERVICE_BUS_KEY_NAME = ""
    AZURE_SERVICE_BUS_ACCESS_KEY = ""
elif ENV == 'prod':
    AZURE_SERVICE_BUS_NAMESPACE = ""
    AZURE_SERVICE_BUS_TOPIC_NAME = ""
    AZURE_SERVICE_BUS_KEY_NAME = ""
    AZURE_SERVICE_BUS_ACCESS_KEY = ""

bus_service = ServiceBusService(
    service_namespace=AZURE_SERVICE_BUS_NAMESPACE,
    shared_access_key_name=AZURE_SERVICE_BUS_KEY_NAME,
    shared_access_key_value=AZURE_SERVICE_BUS_ACCESS_KEY)


# Create a subscription
def create_subscription(name, filter):
    """
    Create a subscription to the Topic
    :param name: the name of the subscription
    :param filter: SQL like filter on the custom_properties of the messages
    """
    sub_name = name
    bus_service.create_subscription(AZURE_SERVICE_BUS_TOPIC_NAME, sub_name)
    # add a filter rule
    rule = Rule()
    rule.filter_type = 'SqlFilter'
    rule.filter_expression = filter
    bus_service.create_rule(AZURE_SERVICE_BUS_TOPIC_NAME, sub_name, sub_name + 'filter', rule)
    bus_service.delete_rule(AZURE_SERVICE_BUS_TOPIC_NAME, sub_name, DEFAULT_RULE_NAME)


# Examples of subscriptions
# This may be a subscription which retrieves ALL messages regarding WP3
create_subscription('wp3', "ireacttask >= 300 AND ireacttask <= 399")
# This subscription retrieves all messages with ireacttask = 459
create_subscription('wp4', "ireacttask = 459")


# To send a message to the Topic:
#   it is important to add ireacttask as a custom property, otherwise this message
#   will not be filtered for the subscriptions
msg = Message('My Custom Message', custom_properties={'ireacttask': 400})
bus_service.send_topic_message(AZURE_SERVICE_BUS_TOPIC_NAME, msg)

# To retrieve a message, specify the subscription name (i.e. the filter to use)
# in case there are no messages available, after 5 seconds this method will return None
subscription_name = 'wp3'
msg = bus_service.receive_subscription_message(AZURE_SERVICE_BUS_TOPIC_NAME, subscription_name, peek_lock=False, timeout=5)
# to load the message into a dictionary
json.loads(msg.body)
