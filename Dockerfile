FROM phusion/baseimage:0.9.22
LABEL maintainers="Stefano Fioravanzo <fioravanzo@fbk.eu>"

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

#Layer for python and gdal support
RUN apt-get update && apt-get install -y software-properties-common curl \
    && add-apt-repository ppa:ubuntugis/ubuntugis-unstable && apt-get update \
    && apt-get install -y python3-pip libssl-dev libffi-dev python3-gdal \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 10 \
    && update-alternatives --install /usr/bin/pip    pip    /usr/bin/pip3    10 \
    && rm -rf /var/lib/apt/lists/*

#Begin of mandatory layers for Microsoft ODBC Driver 13 for Linux
RUN apt-get update && apt-get install -y apt-transport-https wget

#RUN sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/mssql-ubuntu-xenial-release/ xenial main" > /etc/apt/sources.list.d/mssqlpreview.list'
#RUN apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893
#RUN apt-get update -y
#RUN ACCEPT_EULA=Y apt-get install msodbcsql mssql-tools unixodbc-dev 
#RUN apt-get install -y unixodbc-dev-utf16
#RUN ACCEPT_EULA=Y apt-get install msodbcsql

#RUN apt-get install -y locales

RUN sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/mssql-ubuntu-xenial-release/ xenial main" > /etc/apt/sources.list.d/mssqlpreview.list'
	
RUN apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893
RUN apt-get update -y
RUN apt-get install -y libodbc1-utf16 unixodbc-utf16 unixodbc-dev-utf16
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql
#for silent install use ACCEPT_EULA=Y apt-get install msodbcsql
RUN apt-get install -y locales 

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen 

RUN locale-gen
#End of mandatory layers for Microsoft ODBC Driver 13 for Linux

RUN apt-get remove -y curl

# Clean up APT
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#Layers for the django app
RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pip install pip --upgrade
RUN pip install -r requirements.txt
RUN pip install raven --upgrade
RUN pip install gunicorn
RUN pip install whitenoise

# set django settings
ARG DJANGO_SETTINGS
ENV DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS

#Layer for exposing the app through gunicorn
EXPOSE 8002
COPY entrypoint.sh /code/idi/
WORKDIR /code/idi

# CMD ["sh", "entrypoint.sh"]
RUN mkdir -p /etc/my_init.d
COPY entrypoint.sh /etc/my_init.d/entrypoint.sh
  RUN chmod +x /etc/my_init.d/entrypoint.sh
