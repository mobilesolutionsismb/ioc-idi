# IDI

IDI is a data interface and data proxy used to store, query and retrieve heterogeneous data. IDI provides a unified and consistent metadata schema, used to describe the blobs' details (authors, source, functionality, features, etc...). 

Data is aggregated and organized automatically into a single blob storage and it can be retrieved by querying IDI using its custom defined query language. 

IDI also interacts with pub-sub message queueing system, by pushing new messages every time an action (file upload operation, update, delete, ...). This allows partners to build automated systems that react to IDI event based on the published messages. 

More details about storage technologies and architecture used can be found in the *Storage Details* section of this document.

This module is part of the [I-REACT Open Core](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/).

## Acknowledgment

This work was partially funded by the European Commission through the [I-REACT project](http://www.i-react.eu/) (H2020-DRS-1-2015), grant agreement n.700256.

## Deployment

The application can be packaged using Docker. A Dockerfile is present in the root of the project.

To build a specific version of the application, checkout either on the `devel` or `test` branch and run `docker build --build-arg DJANGO_SETTINGS=conf.settings.dev  -f Dockerfile -t ireact-dev .` (Use `conf.settings.test` when building on the `test` environment). When the new image is built, stop the previous one and run it with `docker run -p 8004:8002 --name ireact-dev -it --restart always -d ireact-dev` (NOTE: Be careful with the port mapping. Check the `nginx.conf` for what ports to use based on the environment).

## APIs

The IDI API exposes four end points, mapped to four HTTP methods:

- **Data Upload** (via `POST`): Endpoint devoted to upload a new file to the blob storage along with its corresponding metadata.
- **Data Retrieval** (via `POST`): Used to make query to the I-Reactor on the metadata and retrieve the URLs of the entities stored in the data lake. 
- **Data Update** (via `PUT`): Used to update the metadata uploaded with the `POST` API. 
- **Soft Delete** (via `DELETE`): Used to (soft) delete the metadata present in the database. 

There is also a GET end point `apimetadata/` which returns all the records present in the `IrMetadata` table. This is currently used for testing purposes and may be removed in the future if necessary.

## Storage Details

The application stores the metadata of the binary files into the `IrMetadata` and `IrGeographicBoundingBoxes` tables of the reference database.

The second table is used to store the (possible) multiple geographic bounding boxes defined for a single binary file.

The binary files are stored in the Azure Blob Storage that is configured in the Django settings, inside the `idi` container. 

All the binary files are publicly accessible through their unique URIs of the blob store.

Upon every main event (POST to upload new binary/metadata, POST to query for some files, etc...) the application will queue a message into a Topic of Azure Service Bus (configurable in the Django settings). To access the messages one needs to subscribe to the topic of interest. Each subscription comes with a filter, which lets a subscription read just the messages which match the specified filter.

The sample code below shows how to create a new subscription and retrieve available messages.

Note: creating a subscription is not retroactive, once a message has been posted it will be available just to the subscriptions active at that moment.

```python
from azure.servicebus import ServiceBusService, Message, Topic, Rule, DEFAULT_RULE_NAME
# connect to Azure ServiceBus
bus_service = ServiceBusService(
    service_namespace='<Namespace>',
    shared_access_key_name='AccessPolicy',
    shared_access_key_value='<Key>')
topic_name = "<topic_name>"

# Create a subscription
def create_subscription(name, filter):
    """
    Create a subscription to the Topic
    :param name: the name of the subscription
    :param filter: SQL like filter on the custom_properties of the messages
    """
    sub_name = name
    bus_service.create_subscription(topic_name, sub_name)
    # add a filter rule
    rule = Rule()
    rule.filter_type = 'SqlFilter'
    rule.filter_expression = filter
    bus_service.create_rule(topic_name, sub_name, sub_name + 'filter', rule)
    bus_service.delete_rule(topic_name, sub_name, DEFAULT_RULE_NAME)

# Examples of subscriptions
# This may be a subscription which retrieves ALL messages regarding tasks in the 300s task number
create_subscription('300s', "task >= 300 AND task <= 399")
# This subscription retrieves all messages with task = 459
create_subscription('459', "task = 459")

# To send a message to the Topic:
#   it is important to add task as a custom property, otherwise this message
#   will not be filtered for the subscriptions
msg = Message('My Custom Message', custom_properties={'task': 400})
bus_service.send_topic_message(topic_name, msg)

# To retrieve a message, specify the subscription name (i.e. the filter to use)
# in case there are no messages available, after 5 seconds this method will return None
subscription_name = '300s'
msg = bus_service.receive_subscription_message(topic_name, '300s', peek_lock=False, timeout=5)
# to load the message into a dictionary
json.loads(msg.body)

```

# API Specification

The following examples will show how to send basic requests to the endpoints using Python's library `requests`.

## Authentication

The APIs are **not** publicly accessible. 

In order to make a request, the client first needs to obtain an authentication token. 

For the moment there exists just one test account (username: `test`, password: `test`). To obtain an authentication token just the following code:

```python
import requests
import json

url = "<deployment_url>/api-token-auth/"
response = requests.post(url, data={'username': 'test', 'password': 'test'}).text
token = json.loads(response)['token']
```

In the token variable will be present the authentication token to be used to make the requests. Doing this is fairly easy, just add the `Authorization` header as shown here:

```python
requests.get(url, headers={'Authorization': 'JWT {}'.format(token)})
```

For the moment a single token remains valid for 5 minutes. 

In the future the token's settings and account management will change according the partners' needs.

The next examples will assume a valid token has already been acquired.

## POST (Data Upload)

A proper POST request to upload a binary file will have to include both a single binary file and all the required metadata fields. 
The snippet below describes how to create a simple POST request in Python.

```python
url = "<deployment_url>/apimetadataquery/create/"
file_path = "/path/to/file_name.ext"
metadata = {
	# metadata fields...
    }
files = {'file_name.ext': open(file_path, 'rb').read()}
response = requests.post(url, headers={'Authorization': 'JWT {}'.format(token)}, data=metadata, files=files)
print(response.status_code)
print(response.text)
```

**NOTE**: 

- The keys of the `metadata` dictionary are mapped one-to-one with the column names of the `IrMetadata` table, and the keys of the geographic bounding boxes dictionary are mapped one-to-one with the `IrGeographicBoundingBoxes` table. 
- The key of the `files` dictionary will be the actual name used by the blob storage to store the file, so set it accordingly.
- If there is any parsing/value error in the request, the IDI will respond with a message indicating what is the error.

## POST (Data Retrieval)

In order to retrieve files present in the blob storage, the GET API lets the client send a query to filter the metadata. 
The query will be written using a meta-language consisting of possibly-nested lists of query filter descriptions that use the Django notation, 
which will be then translated to SQL.

#### Filtering

Each filter description is a list.  The first element of the list is always the filter operator name. 

This name is one of either a filter operator, namely `eq` (a synonym for `exact`), or the boolean operators `and`, `or`, and `not`.
    
Primitive query filters have three elements:
    
`[filteroperator, fieldname, queryarg]`

`filteroperator` must be one of `in`, `range`, `exact`, `iexact`, `lt`, `lte`, `gt`, `gte`.

`fieldname` is the metadata field being queried. `queryarg` is the argument on which the filter is applied.
    
`and` and `or` query filters are lists that begin with the appropriate 
operator name, and include subfilters as additional list elements:
    
`['or', [subfilter1], [subfilter2], ...]`

`['and', [subfilter1], [subfilter2], ...] `
    
`not` query filters consist of exactly two elements:
    
`['not', [subfilter]]`
    
Example queries:

```python
['and', 
	['in', 'col1', ['string1', 'string2']],
	['in', 'col2', ['string3']]
]
```
 
The values for some of the fields have to respect the Enumerators specified here 
`https://ireactbe-test.azurewebsites.net/api/services/app/Enums/GetEnums`. 

Upon receiving the request, the application will parse and map these field to the corresponding integer values.

The snippet below shows an example of a Python client requesting the retreival of some data using operators:

```python
url = "http://idi-ireact.azurewebsites.net/apimetadataquery/retrieve/"

req = {"query": ["and",
                    ["in", "identification_resourcetype", ["service", "series"]],
                    ["in", "conformity_degree", ["conformant"]]
                ]
        }
response = requests.get(url, headers={'Authorization': 'JWT {}'.format(token)}, json=req)
print(response.status_code)
# The links to the binary files with metadata that respect the query are returned in a simple list.
print(response.text)
```

At the moment spatial operations are not available on the geographic bounding boxes. Normal queries can still be performed on the four fields that define the bounding boxes present in the `IrGeographicBoundingBoxes` table. In the case a single binary file comprises more than one bounding box the query will match independently each bounding box with the same filter.

## PUT (Data Update)

A proper request to update the data present in the `Metadata` table of the database includes a dictionary with all the fields that are to be updated. You can see below a simple example:

```python
# we use here a dummy metadataid: 1111
url = "<deployment_url>/apimetadataquery/update/1111/"
# dictionary with the fields to update
update_metadata = {
        # metadata fields...
    }
# send the PUT request
r = requests.put(url_create, headers={'Authorization': 'JWT {}'.format(token)}, data=metadata)
print(response.status_code)
```

## DELETE (Soft Delete)

A soft delete just assigns the `isDeleted` column of the specified row to `True` and sets the `deletionTime` to the current timestamp. This update is done also to the referenced records of the `GeographicBoundingBoxes` table.

The binary files associated to the records are left untouched.

```python
URL = "<deployment_url>/apimetadataquery/softdelete/<metadataid>/"
# send the delete req	uest
r = requests.delete(url_create, headers={'Authorization': 'JWT {}'.format(token)})
print(r.status_code)
```

--

This work was partially funded by the European Commission through the I-REACT project (H2020-DRS-1-2015), grant agreement n.700256.